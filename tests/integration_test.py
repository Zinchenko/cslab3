# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long

import contextlib
import io
import os
import tempfile
import unittest

import machine
import translator


class TestMachine(unittest.TestCase):

    def test_hello(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "code/hello_world.txt"
            target = os.path.join(tmpdirname, "machine_code.out")

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, target])

            self.assertEqual(stdout.getvalue(),
                             'source LoC: 32 code instr: 21\nhello world\ninstr_counter:  20 ticks: 127\n')

    def test_cat(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "code/cat.txt"
            target = os.path.join(tmpdirname, "machine_code.out")
            input_stream = "code/test.txt"

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                with self.assertLogs('', level='INFO') as logs:
                    translator.main([source, target])
                    machine.main([target, input_stream])

            self.assertEqual(stdout.getvalue(),
                             'source LoC: 11 code instr: 6\nHello World from input!\ninstr_counter:  71 ticks: 378\n')

            self.assertEqual(logs.output,
                             ['WARNING:root:Input buffer is empty!',
                              "INFO:root:output_buffer: 'Hello World from input!'"])
