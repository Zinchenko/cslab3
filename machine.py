#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=invalid-name
# pylint: disable=consider-using-f-string
# pylint: disable=import-error
# pylint: disable=too-few-public-methods

import logging
import sys
import translator
from isa import Opcode, OperandType


class Alu:
    def __init__(self):
        self.left: int = 0
        self.right: int = 0
        self.operations: dict = {
            Opcode.DIV: lambda left, right: left / right,
            Opcode.MOD: lambda left, right: left % right,
            Opcode.CMP: lambda left, right: left - right,
            Opcode.ADD: lambda left, right: left + right,
            Opcode.SUB: lambda left, right: right - left,
            Opcode.MUL: lambda left, right: left * right
        }


class DataPath:
    def __init__(self, program, data_memory_size, input_buffer):
        self.data_memory: list[int] = [0] * data_memory_size
        self.data_address: int = 0
        self.program: list = program
        self.mem()
        self.acc: int = 0
        self.input_buffer: list[str] = input_buffer
        self.output_buffer: list[str] = []
        self.alu: Alu = Alu()
        self.operand: int = 0

    def mem(self):
        for i in range(len(self.program)):
            self.data_memory[i + 128] = self.program[i]

    def latch_data_addr(self, operand_sel: bool):
        if operand_sel:
            self.data_address = self.operand
        else:
            self.data_address += 1

        if not 0 <= self.data_address < len(self.data_memory):
            raise AssertionError("out of memory: {}".format(self.data_address))

    def latch_acc(self, mem_sel: OperandType):
        if mem_sel not in {OperandType.ADDR, OperandType.NUM, OperandType.REG}:
            raise AssertionError("INTERNAL ERR")
        if mem_sel is OperandType.ADDR:
            if self.data_address == 254:
                if len(self.input_buffer) == 0:
                    raise EOFError()
                symbol = self.input_buffer.pop(0)
                symbol_code = ord(symbol)
                if symbol_code > 2147483647 or -2147483648 > symbol_code:
                    raise AssertionError("TOO BIG INPUT")
                logging.debug('input: %s', repr(symbol))
                self.acc = symbol_code
            else:
                self.acc = self.data_memory[self.data_address]
        else:
            self.acc = self.operand

    def latch_alu(self, sel_left, mem_sel: OperandType):
        if mem_sel not in {OperandType.ADDR, OperandType.NUM, OperandType.REG}:
            raise AssertionError("INTERNAL_ERR")
        if sel_left:
            if mem_sel is OperandType.ADDR:
                self.alu.left = self.data_memory[self.data_address]
            else:
                self.alu.left = self.operand
        else:
            self.alu.right = self.acc

    def execute_alu(self, opcode: Opcode):
        res = self.alu.operations[opcode](self.alu.left, self.alu.right)
        while res > 2147483647:
            res = -2147483648 + (res - 2147483647)
        while res < -2147483648:
            res = 2147483647 - (res + 2147483648)

        self.acc = res

    def wr(self):
        if self.data_address == 255:
            symbol = self.acc if self.acc > 256 else chr(self.acc)
            logging.debug('output: %s << %s', repr(
                ''.join(self.output_buffer)), repr(symbol))
            self.output_buffer.append(str(symbol))
        else:
            self.data_memory[self.data_address] = self.acc

    def zero(self):
        return self.acc == 0


class ControlUnit:
    def __init__(self, data_path):
        self.program_counter: int = 0
        self.data_path: DataPath = data_path
        self._tick: int = 0

    def tick(self) -> None:
        self._tick += 1

    def current_tick(self):
        return self._tick

    def latch_program_counter(self, sel_next: bool) -> None:
        if sel_next:
            self.program_counter += 1
        else:
            self.program_counter = self.data_path.data_memory[self.program_counter + 128]

    def set_alu_val(self, is_left: bool, op_type: OperandType):
        self.latch_program_counter(sel_next=True)
        self.tick()
        if op_type is OperandType.REG:
            self.data_path.latch_alu(False, mem_sel=op_type)
            self.tick()
        elif op_type in {OperandType.ADDR, OperandType.NUM}:
            self.data_path.operand = self.data_path.data_memory[self.program_counter + 128]
            if op_type is OperandType.ADDR:
                self.data_path.latch_data_addr(operand_sel=True)
                self.tick()
            if not is_left:
                self.data_path.latch_acc(op_type)
                self.tick()
            self.data_path.latch_alu(is_left, op_type)
            self.tick()

            self.data_path.operand = 0

    def decode_and_execute_instruction(self):
        instr: dict = self.data_path.data_memory[self.program_counter + 128]
        opcode: Opcode = instr["opcode"]
        operands: list[OperandType] = instr["operands"]

        if opcode is Opcode.EXIT:
            raise StopIteration()
        if opcode is Opcode.JMP:
            self.latch_program_counter(sel_next=True)
            self.tick()
            self.latch_program_counter(sel_next=False)
            self.tick()
        elif opcode is Opcode.MOV:
            for i in range(2):
                op_type = OperandType(operands[i])
                self.latch_program_counter(sel_next=True)
                self.tick()
                if op_type in {OperandType.NUM, OperandType.ADDR}:
                    self.data_path.operand = self.data_path.data_memory[self.program_counter + 128]
                    if op_type == OperandType.ADDR:
                        self.data_path.latch_data_addr(operand_sel=True)
                        self.tick()
                    if i == 0:
                        self.data_path.latch_acc(op_type)
                    else:
                        self.data_path.wr()
                    self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()
        elif opcode in {Opcode.BEQ, Opcode.BNE}:
            if self.data_path.zero() ^ opcode:
                self.latch_program_counter(sel_next=True)
                self.tick()
                self.latch_program_counter(sel_next=False)
                self.tick()
                return
            self.latch_program_counter(sel_next=True)
            self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()
        else:
            self.set_alu_val(is_left=True, op_type=OperandType(operands[0]))
            self.set_alu_val(is_left=False, op_type=OperandType(operands[1]))
            self.data_path.execute_alu(opcode)
            self.tick()
            if opcode in {Opcode.ADD, Opcode.SUB} and OperandType(operands[1]) is OperandType.ADDR:
                self.data_path.wr()
                self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()

    def __repr__(self):
        state = "{{TICK: {}, ADDR: {}, OUT: {}, ACC: {}}}".format(
            self._tick,
            self.data_path.data_address,
            self.data_path.data_memory[self.data_path.data_address],
            self.data_path.acc,
        )

        instr = self.data_path.data_memory[self.program_counter + 128]
        opcode = instr["opcode"]
        args = []

        for i in range(2):
            if instr["operands"][i] != OperandType.NONE:
                args.append(self.data_path.data_memory[self.program_counter + 128 + i + 1])
        action = "{} {}".format(Opcode(opcode).name, str(args))

        return "{} {}".format(action, state)


def simulation(code, input_tokens, data_memory_size, limit):
    data_path = DataPath(code, data_memory_size, input_tokens)
    control_unit = ControlUnit(data_path)
    instr_counter = 0

    try:
        logging.debug('%s', control_unit)
        while True:
            if limit <= instr_counter:
                raise AssertionError("TOO LONG EXECUTE")
            control_unit.decode_and_execute_instruction()
            instr_counter += 1
            logging.debug('%s', control_unit)
    except EOFError:
        logging.warning('Input buffer is empty!')
    except StopIteration:
        pass

    logging.info('output_buffer: %s', repr(''.join(data_path.output_buffer)))

    return ''.join(data_path.output_buffer), instr_counter, control_unit.current_tick()


def main(args):
    if len(args) != 2:
        raise AssertionError("Wrong arguments: machine.py <code_file> <input_file>")

    code_file, input_file = args

    with open(code_file, encoding="utf-8") as file:
        raw_code = file.read()

    code = translator.deserialize_code(raw_code)

    with open(input_file, encoding="utf-8") as file:
        input_text = file.read()
        input_token = []
        for char in input_text:
            input_token.append(char)

    output, instr_counter, ticks = simulation(code, input_tokens=input_token, data_memory_size=256, limit=10000)

    print(''.join(output))
    print("instr_counter: ", instr_counter, "ticks:", ticks)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv[1:])
