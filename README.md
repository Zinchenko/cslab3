# Assembler. Транслятор и модель

- Зинченко Константин Сергеевич, гр. P33121
- `asm | cisc | neum | hw | instr | binary | stream | port | prob1`

## Язык программирования

``` bnf
program ::= line "\n" program

line ::= | label | comment | command | section

label ::= name ":"

comment ::= ; <any sequence not containing ';'>

command ::= "\t" operation_2_args " " operand ", " operand |
            "\t" operation_1_arg " " operand |
            "\t" operation_0_args |

section ::= "section ." section_name

section_name ::= "text" | "data"

operation_2_args ::= "add" | "sub" | "mov" | "div" | "mod" | "cmp"

operation_1_arg ::= "beq" | "bne" | "jmp" | "word" | "print" | "read"

operation_0_args ::= "exit"

operand ::= name | number

name ::= [a-z_]+
          
number ::= [-2^32; 2^32 - 1]

```

## Модель процессора

Реализовано в модуле: [machine](machine.py).

### Схема DataPath и ControlUnit

![https://gitlab.se.ifmo.ru/Zinchenko/cslab3/chema.jpg](/chema.jpg "Схема DataPath и ControlUnit")

- `latch_data_addr` -- защёлкнуть в `data_addr` инкриминированное значение, либо адрес из `Program memory`;
- `latch_acc` -- защёлкнуть в аккумулятор выход `ALU`, выход `Data memory`, либо операнд из `Program memory`;
- `latch_alu_right` -- защёлкнуть в правый вход `ALU` значение аккумулятора
- `latch_alu_left` -- защёлкнуть в левый вход `ALU` выход `Data memory`, либо операнд из `Program memory`
- `wr` -- записать значение аккумулятора в память.

Флаги:

- `zero` -- отражает наличие нулевого значения в аккумуляторе.

## ControlUnit

Реализован в классе `ControlUnit`.

- Hardwired (реализовано полностью на python).
- Моделирование на уровне инструкций.
- Трансляция инструкции в последовательность сигналов: `decode_and_execute_instruction`.

Сигнал:

- `latch_program_couter` -- сигнал для обновления счётчика команд в ControlUnit.

Особенности работы модели:

- Для журнала состояний процессора используется стандартный модуль logging.
- Количество инструкций для моделирования ограничено hardcoded константой.
- Остановка моделирования осуществляется при помощи исключений:
    - `EOFError` -- если нет данных для чтения из порта ввода-вывода;
    - `StopIteration` -- если выполнена инструкция `exit`.
- Управление симуляцией реализовано в функции `simulate`.

Код выполняется последовательно. Операции:

- `add <arg1> <arg2>` -- прибавить ко второму аргументу первый
- `sub <arg1> <arg2>` -- вычесть из второго аргумента первый
- `mov <arg1> <arg2>` -- скопировать значение из первого аргумента во второй
- `div <arg1> <arg2>` -- получить целую часть от деления первого аргумента на второй
- `mod <arg1> <arg2>` -- получить остаток от деления первого аргумента на второй
- `mul <arg1> <arg2>` -- получить произведение первого аргумента на второй
- `cmp <arg1> <arg2>` -- получить результат сравнения первого аргумента со вторым (0, если аргументы равны)
- `beq <label>` -- если значение в аккумуляторе равно нулю, перейти на аргумент-метку
- `bne <label>` -- если значение в аккумуляторе отлично от нуля, перейти на аргумент-метку
- `jmp <label>` -- безусловный переход на аргумент-метку
- `print <arg>` -- распечатать в поток вывода значение из аргумента
- `read <arg>` -- прочитать в аргумент значение из потока ввода
- `word <number>` -- объявление переменной
- `exit` -- завершить выполнение программы

Поддерживаемые аргументы:

- название **объявленной** метки
- число в диапазоне [-2^32; 2^32 - 1]

Дополнительные конструкции:

- `; <any sequence not containing ';'>` - комментарий
- `section .text` - объявление секции кода
- `section .data` - объявление секции данных
- `<label>:` - метки для переходов / названия переменных

*Примечания:

- Результаты операций `add` и `sub` помещаются во второй аргумент, в остальных случаях - в `acc`
- Должна присутствовать метка `start` - точка входа в программу

## Система команд

Особенности процессора:

- Машинное слово -- 32 бита, знаковое.
- Память данных:
    - адресуется через регистр `data_address` (может быть инкриминирован или перезаписан операндом инструкции);
    - может быть записана из аккумулятора `acc`;
    - может быть прочитана в левый вход АЛУ, либо в аккумулятор `acc`;
    - имеет зарезервированные ячейки для подключения потоков ввода-вывода.
- Регистр аккумулятора: `acc`:
    - используется как флаг (сравнение с 0);
    - см. память данных.
- Ввод-вывод -- memory-mapped через резервированные ячейки памяти, символьный.
- `program_counter` -- счётчик команд:
    - инкриминируется после каждой инструкции или перезаписывается инструкцией перехода.

### Набор инструкции

| Syntax                        | Mnemonic                      | Ticks | Comment                            |
|:------------------------------|:------------------------------|-------|:-----------------------------------|
| `add/sub <arg> <arg>`         | add/sub `<arg> <arg>`         | 7     | кол-во тактов зависит от операндов |
| `mov <arg> <arg>`             | mov `<arg> <arg>`             | 4     | кол-во тактов зависит от операндов |
| `div/mod/mul/cmp <arg> <arg>` | div/mod/mul/cmp `<arg> <arg>` | 6     | кол-во тактов зависит от операндов |
| `beq/bne/jmp <arg>`           | beq/bne/jmp `<arg>`           | 2     | cм. язык                           |
| `exit`                        | exit                          | 0     | см. язык                           |
| `word <arg>`                  | mov `<arg> <mem>`             | 6     | адрес определяется транслятором    |
| `print <arg>`                 | mov `<arg> <mem_size>`        | 5     | запись данных                      |
| `read <arg>`                  | mov `<mem_size - 1> <arg>`    | 5     | чтение данных                      |

## Транслятор

Интерфейс командной строки: `translator.py <input_file.asm> <target_file.bin>"`

Реализовано в модуле: [translator](translator.py)

Этапы трансляции (функция `translate`):

1. Индексация всех меток в программе (меток данных и команд)
2. Трансформирование текста в последовательность значимых термов.
3. Проверка корректности программы.
4. Подстановка адресов вместо меток.
5. Генерация машинного кода.

Правила генерации машинного кода:

- один символ языка -- одна инструкция;
- для команд, однозначно соответствующих инструкциям -- прямое отображение;
    - в отличие от остальных инструкций, инструкция `mov` служит отображением сразу для **нескольких** команд
- первые `N` инструкций в коде - инициализация `N` переменных
- `N+1`-я инструкция перехода к метке start

### Кодирование инструкций

- Последовательность термов сериализуется в бинарный код.
- Инструкции переменной длины: 1 - 9 байт:
    - один байт на метаданные инструкции
    - от нуля до двух 4-байтовых значений под операнды.
    - количество 4-байтовых значений определяется мнемоникой команды и типом операндов
        - для операндов типа `OperandType.REG` и `OperandType.NONE`(отсутствие операнда) ячейки не предоставляются
- Десереализуются с учетом количества операндов и последовательно загружаются в память инструкций:
    - головной байт в одну ячейку
    - каждый из операндов в следующие ячейки

Коды операций и типы операндов перечислены в модуле [isa](isa.py), где:

- `Opcode` -- перечисление кодов операций;
    - сейчас схема рассчитана на максимум 16 операций, но легко масштабируется, так как запас на ячейку в памяти команд
      еще 3 байта.
- `OperandType` -- перечисление типов операндов;
    - аналогично, схема рассчитана на 4 типа операндов, но есть запас под масштабируемость
- `Term` -- структура для описания значимого фрагмента кода исходной программы.

Пример (для `сode/prob1.text`):

```commandline
$ py ./translator.py code/prob1.txt code/binary.txt
source LoC: 25 code instr: 15
```

## Апробация

В качестве тестов использовано два алгоритма:

1. [hello world](code/hello_world.txt).
2. [cat](code/cat.txt) -- программа `cat`, повторяем ввод на выводе.

Интеграционные тесты реализованы тут: [integration_test](tests/integration_test.py)

CI:

```yaml
lab3-example:
  stage: test
  image:
    name: python-tools
    entrypoint: [ "" ]
  script:
    - python3-coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t python3-coverage report
    - find . -type f -name "*.py" | xargs -t pep8 --ignore=E501
    - find . -type f -name "*.py" | xargs -t pylint --disable C0301,W0621,R0912,C0114,C0115,R0902,C0200
```

| ФИО            | алг.  | LoC | code байт | code инстр. | инстр. | такт. |
|----------------|-------|-----|-----------|-------------|--------|-------|
| Зинченко К. С. | hello | 32  | 195       | 21          | 20     | 1127  |
| Зинченко К. С. | cat   | 11  | 38        | 6           | 71     | 378   |
| Зинченко К. С. | prob1 | 24  | 107       | 15          | 7264   | 42249 |


