#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=import-error
# pylint: disable=missing-module-docstring

import sys
import re
from isa import Term, Operand, OperandType, instr_info, Opcode


def get_bin(num, bits):
    return format(int(num), 'b').zfill(bits)


def serialize_term(term: Term, instr_info: dict) -> str:
    result = get_bin(instr_info[term.operation]["opcode"], 4)
    op_count = len(term.operands)
    for operand in term.operands:
        result += get_bin(operand.type, 2)
    result += get_bin(OperandType.NONE, 2) * (2 - op_count)
    for operand in term.operands:
        if operand.type in {OperandType.NUM, OperandType.ADDR}:
            result += get_bin(operand.value, 32)
    return result


def serialize_vars(vars_mem: list[int]) -> list[str]:
    result = [get_bin(len(vars_mem), 8)]
    for var in vars_mem:
        result.append(get_bin(var, 32))
    return result


def deserialize_code(code: str):
    instr_list = []
    while code:
        instr: dict = {"opcode": Opcode(int(code[:4], 2)), "operands": []}
        code = cut_str_front(code, 4)

        for i in range(2):
            instr["operands"].append(OperandType(int(code[:2], 2)))
            code = cut_str_front(code, 2)
        instr_list.append(instr)

        for i in range(2):
            if instr["operands"][i] in {OperandType.NUM, OperandType.ADDR}:
                op_val = int(code[:32], 2)
                instr_list.append(op_val)
                code = cut_str_front(code, 32)

    return instr_list


def cut_str_front(string: str, length_to_cut) -> str:
    return string[length_to_cut:]


def is_label(string) -> bool:
    return re.match(r"^[a-z_]+:$", string) is not None


def is_section(string) -> bool:
    return re.match(r"^section \.[a-z]+$", string) is not None


patterns = [r"^\t[a-z]+ *$", r"^\t[a-z]+ [a-z0-9_]+ *$", r"^\t[a-z]+ [a-z0-9_]+, [a-z0-9_]+ *$"]


def validate_operation_line(line) -> list:
    command = line.split(";")[0]
    parts = line.split()
    instr = parts[0]

    if instr not in instr_info:
        raise AssertionError("UNKNOWN OPERATION")

    if not len(command.split()) - 1 == instr_info[instr]["operands"]:
        raise AssertionError("WRONG ARGS NUM")

    if not re.match(patterns[instr_info[instr]["operands"]], command):
        raise AssertionError("INVALID_SYNTAX")

    if instr_info[parts[0]]["operands"] == 2:
        parts[1] = parts[1][:-1]

    return parts[:instr_info[instr]["operands"] + 1]


def validate_section_line(line, available_sections) -> str:
    if not is_section(line):
        raise AssertionError("INVALID_SYNTAX")
    section = line.split(".")[1]

    if section not in available_sections:
        raise AssertionError("UNKNOWN_SECTION")
    available_sections.remove(section)

    return section


def validate_operands(term, text_labels: dict, data_labels: dict):
    if term.operation in {"add", "sub"} \
            and term.operands[1].type not in {OperandType.ADDR, OperandType.REG}:
        raise AssertionError
    if term.operation in {"bne", "beq", "jmp"} \
            and (term.operands[0].value not in text_labels.keys() or term.operands[0].type is not OperandType.ADDR):
        raise AssertionError
    if term.operation == "read" and \
            (term.operands[1].type not in {OperandType.ADDR, OperandType.REG} or term.operands[
                1].type is OperandType.ADDR and term.operands[1].value not in data_labels.keys()):
        raise AssertionError
    if term.operation == "print" \
            and term.operands[0].type is OperandType.ADDR and term.operands[0].value not in data_labels.keys():
        raise AssertionError


def index_labels(raw_code: list):
    text_labels: dict = {}
    data_labels: dict = {}

    non_blank_lines_cnt: int = 0
    cur_section: str = ""
    mem_pos: int = 0

    available_sections = {"data", "text"}

    for line in raw_code:
        if line.strip() == '' or line.strip()[0] == ";":
            continue

        if not cur_section:
            cur_section = validate_section_line(line, available_sections)
        else:
            if is_label(line):
                if cur_section == "text":
                    if line[:-1] in text_labels:
                        raise AssertionError("DUPLICATE_LABEL")
                    text_labels[line[:-1]] = non_blank_lines_cnt
                else:
                    if line[:-1] in data_labels:
                        raise AssertionError("DUPLICATE_LABEL")
                    data_labels[line[:-1]] = mem_pos
                    mem_pos += 1
            elif is_section(line):
                cur_section = validate_section_line(line, available_sections)
            else:
                parts = validate_operation_line(line)
                non_blank_lines_cnt += 1 + instr_info[parts[0]]["operands"]
                if parts[0] in {"print", "read"}:
                    non_blank_lines_cnt += 1
    return text_labels, data_labels


def fill_terms(raw_code: list, text_labels: dict, data_labels: dict):
    terms: list[Term] = []
    vars_mem_idx = 0

    for line in raw_code:

        if line.strip() == '' or line.strip()[0] == ";" or is_label(line) or is_section(line):
            continue

        parts = validate_operation_line(line)
        instr_name = parts[0]
        term = Term(instr_name)

        for i in range(instr_info[instr_name]["operands"]):
            if parts[1 + i] in list(text_labels.keys()) + list(data_labels.keys()):
                op_type = OperandType.ADDR
            elif parts[1 + i].isdigit():
                op_type = OperandType.NUM
                if int(parts[1 + i]) > 2147483647 or -2147483648 > int(parts[1 + i]):
                    raise AssertionError("TOO BIG INPUT")
            else:
                raise AssertionError("INVALID_OPERAND")

            term.operands.append(Operand(op_type, parts[1 + i]))
        if instr_name == "read":
            term.operands.append(term.operands[0])
            term.operands[0] = (Operand(OperandType.ADDR, 254))
        elif instr_name == "print":
            term.operands.append(Operand(OperandType.ADDR, 255))
        elif instr_name == "word":
            term.operands.append(Operand(OperandType.ADDR, vars_mem_idx))
            vars_mem_idx += 1

        try:
            validate_operands(term, text_labels, data_labels)
        except AssertionError as exc:
            raise AssertionError("INVALID_OPERAND") from exc

        if instr_name == "word":
            terms.insert(0, term)
            continue
        terms.append(term)

    if "start" not in text_labels:
        raise AssertionError("MISSING_START_LABEL")
    terms.insert(vars_mem_idx, Term("jmp", [Operand(OperandType.ADDR, "start")]))

    return terms, vars_mem_idx


def translate(text):
    raw_code = list(text.split('\n'))
    text_labels, data_labels = index_labels(raw_code)
    terms, vars_count = fill_terms(raw_code, text_labels, data_labels)

    code = []
    for term in terms:
        for operand in term.operands:
            cur_op = term.operands[term.operands.index(operand)]
            if operand.type == OperandType.ADDR and not str(operand.value).isdigit():
                if operand.value in text_labels:
                    cur_op.value = text_labels[operand.value] + 3 * vars_count + 2
                else:
                    cur_op.value = data_labels[operand.value]
            elif operand.type == OperandType.REG:
                cur_op.value = 0

        code.append(serialize_term(term, instr_info))
    return code


def main(args):
    if len(args) != 2:
        raise AssertionError("Wrong arguments: translator.py <asm_file> <target>")

    source, target = args

    with open(source, "rt", encoding="utf-8") as file:
        source = file.read()

    code = translate(source)
    print("source LoC:", len(source.split("\n")), "code instr:", len(code))

    with open(target, "w", encoding="utf-8") as file:
        for instr in code:
            file.write(instr)


if __name__ == '__main__':
    sys.path.append('.')
    main(sys.argv[1:])
